const express = require('express');
const app = express();
const router = express.Router();

const path = __dirname + '/views/';

module.exports = app;

router.use(function (req, res, next) {
    console.log('/' + req.method + 'request received');
    next();
});

router.get('/', function (req, res) {
    console.log('GET / request handled');
    res.sendFile(path + 'index.html');
});

router.get('/sharks', function (req, res) {
    res.sendFile(path + 'sharks.html');
});

app.use(express.static(path));
app.use('/', router);
