const request = require('supertest');
const app = require('../app');
const http = require('http');

// yogesh  change
describe('Server listening test', () => {
    let server;
    beforeEach(done => {
        server = http.createServer(app);
        server.listen(done)
    });

    afterEach(done => {
        server.close(done);
    });


    test('It should respond with status code 200 for root path', async () => {
        const response = await request(app).get('/');
        expect(response.statusCode).toBe(200);
    });

    test('It should respond with status code 200 for /sharks path', async () => {
        const response = await request(app).get('/sharks');
        expect(response.statusCode).toBe(200);
    });
});